import react from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import './Button.scss';

export default function Button({ type, children, className, ...rest }) {
    return (
        <button {...rest} type={type} className={clsx('button', className)}>
            {children}
        </button>
    );
}

Button.propTypes = {
    type: PropTypes.oneOf('button', 'submit')
}

Button.defaultProps = {
    type: 'button'
}