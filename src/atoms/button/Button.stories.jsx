import React from 'react';

import Button from './Button';

function Template({ type, label, ...rest }) {
    return <Button {...rest} type={type}>{label}</Button>
};

export default {
    title: 'Atoms/Button',
    component: Template
};

export const Base = Template;
Base.args = {
    type: 'button',
    label: 'Button',
};

export const Disabled = Template.bind({});
Disabled.args = {
    ...Base.args,
    disabled: true
};