import React from 'react';

import MenuBar from './MenuBar';
import MenuItem from './MenuItem';

function Template({ ...rest }) {
    return (
        <MenuBar {...rest}>
            <MenuItem>
                Item 1
            </MenuItem>
            <MenuItem>
                Item 2
            </MenuItem>
        </MenuBar>
    )
};

export default {
    title: 'Atoms/MenuBar',
    component: Template
};

export const Base = Template;
Base.args = {
};