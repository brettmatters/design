import react from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import './MenuBar.scss';

export default function MenuBar({ children, className, ...rest }) {
    return (
        <nav {...rest} className={clsx('menu-bar', className)}>
            {children.length > 0 && (
                <ul className={clsx('menu-bar__list', className && `${className}__list`)}>
                    {children}
                </ul>
            )}

        </nav>
    );
}

MenuBar.propTypes = {
}

MenuBar.defaultProps = {
}