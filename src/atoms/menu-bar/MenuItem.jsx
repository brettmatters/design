import react from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import './MenuItem.scss';

export default function MenuItem({ children, className, ...rest }) {
    return (
        <li {...rest} className={clsx('menu-bar__item', className)}>
            {children}
        </li>
    );
}

MenuItem.propTypes = {
}

MenuItem.defaultProps = {
}